# A Terraform Example

## Instructions
1. Create environment variables for your AWS credentials
```
export AWS_ACCESS_KEY_ID=(your access key id)
export AWS_SECRET_ACCESS_KEY=(your secret access key)
export AWS_DEFAULT_REGION=(desired AWS region)
```
1. Initialize terraform
```
./terraform init
```
1. Spin up instance with terraform
```
./terraform apply
```

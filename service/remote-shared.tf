data "terraform_remote_state" "shared" {
  backend = "s3"

  config = {
    bucket = "kasza-terraform-example"
    key    = "shared/terraform.tfstate"
    region = "us-east-1"
  }
}

terraform {
  backend "s3" {
    bucket         = "kasza-terraform-example"
    key            = "service/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform-lock"
  }
}

output "dbusername" {
  value = "${aws_db_instance.db_instance.username}"
}

output "dbpassword" {
  value = "${aws_db_instance.db_instance.password}"
}

output "url" {
  value = "${aws_alb.alb.dns_name}"
}

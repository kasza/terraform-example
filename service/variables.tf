/*
 * AWS Zone ID in Route 53
 * Get the Zone ID for the already existing zone from the AWS Console
 */
variable "aws_zone_id" {
  default = "replaceme"
}

/*
 * Fully qualified host name to be added to Route 53 zone
 */
variable "aws_hostname" {
  default = "aws-example.kasza.ca"
}
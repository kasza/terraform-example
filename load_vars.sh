#!/bin/bash

dotenv_file=".env"
while IFS='=' read key value; do
  if [ ! -z $key ]; then
    declare $key=$value
    export $key
  fi 
done < "$dotenv_file" 

variable "aws_zones" {
 type        = "list"
 description = "List of availability zones to use"
 default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}
